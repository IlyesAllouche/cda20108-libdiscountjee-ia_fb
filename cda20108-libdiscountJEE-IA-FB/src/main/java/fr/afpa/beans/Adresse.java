package fr.afpa.beans;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@RequiredArgsConstructor
@ToString


@Entity
public class Adresse {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "idadresse")
	private int id;

	@NonNull
	@Column(name = "numero")
	private String numero;

	@NonNull
	@Column(name = "nom")
	private String nom;

	@NonNull
	@Column(name = "ville")
	private String ville;
	

	@OneToOne(mappedBy = "adresse")
    private Utilisateur utilisateur;
	
}
