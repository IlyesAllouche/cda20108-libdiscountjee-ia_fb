package fr.afpa.beans;

import java.time.LocalDate;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@RequiredArgsConstructor
@ToString
@Entity
@NamedQuery(name = "annonceFindById", query = "from Annonce a where a.id = :param")
@NamedQuery(name = "annonceFindByName", query = "from Annonce a where nom like concat(:param, '%') ")
@NamedQuery(name = "annonceFindByLvl", query = "from Annonce a where niveau like concat(:param, '%') ")
public class Annonce {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "idannonce")
	private int id;

	@NonNull
	@Column(name = "nom")
	private String nom;

	@NonNull
	@Column(name = "niveau")
	private String niveauScolaire;

	@NonNull
	@Column(name = "isbn")
	private String isbn;

	@NonNull
	@Column(name = "date_edition")
	private LocalDate dateEdition;

	@NonNull
	@Column(name = "maison_edition")
	private String maisonEdition;

	@NonNull
	@Column(name = "prix_unitaire")
	private float prixUnitaire;

	@NonNull
	@Column(name = "remise")
	private int remise;

	@NonNull
	@Column(name = "prix_total")
	private float prixTotal;

	@NonNull
	@Column(name = "quantite")
	private int quantite;	

	
	@Column(name = "photo1")
	private String photo1;

	
	@Column(name = "photo2")
	private String photo2;

	@Column(name = "photo3")
	private String photo3;
	
	@ManyToOne 
	@JoinColumn( name="idUser" )
    private Utilisateur utilisateur;
}
