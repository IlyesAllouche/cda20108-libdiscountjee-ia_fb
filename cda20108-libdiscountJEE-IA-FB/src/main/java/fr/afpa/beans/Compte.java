package fr.afpa.beans;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@RequiredArgsConstructor
@ToString


@Entity
public class Compte {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "idcompte")
	private int idCompte;

	@NonNull
	@Column(name = "login")
	private String login;

	@NonNull
	@Column(name = "mdp")
	private String mdp;
	
	@OneToOne(mappedBy = "compte")
    private Utilisateur utilisateur;
}
