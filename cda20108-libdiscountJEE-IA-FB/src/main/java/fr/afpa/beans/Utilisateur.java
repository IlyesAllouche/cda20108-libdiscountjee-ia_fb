package fr.afpa.beans;


import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@RequiredArgsConstructor
@ToString


@Entity
@NamedQuery(name = "findById", query = "from Utilisateur u where u.id = :param")
public class Utilisateur {
		
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "iduser")
	private int id;
	
	@NonNull
	@Column(name = "nom")
	private String nom;

	@NonNull
	@Column(name = "prenom")
	private String prenom;

	@NonNull
	@Column(name = "mail")
	private String mail;
	
	@NonNull
	@Column(name = "telephone")
	private String tel;

	@NonNull
	@Column(name = "librairie")
	private String nomLibrairie;
	
	
	@Column(name = "desactive")
	private int desactive;

	
	@Column(name = "admin")
	private int admin;

	
	@OneToOne(cascade = {CascadeType.ALL})
    @JoinColumn(name = "adresse_fk")
	private Adresse adresse;
	

	
	@OneToOne(cascade = {CascadeType.ALL})
	@JoinColumn(name = "compte_fk")
	private Compte compte;
		
		
}
