package fr.afpa.dao;

import java.util.ArrayList;

import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.Transaction;

import fr.afpa.beans.Annonce;
import fr.afpa.beans.Utilisateur;
import fr.afpa.utils.HibernateUtils;

public class DaoAnnonce {
	
	private static Session session = null;

	public static void creerAnnonce(Annonce annonce) {
		
		 session = HibernateUtils.getSession();
	       
		 Transaction tx = session.beginTransaction();
		 
		 session.save(annonce);
		 
		 tx.commit();
		 session.close();
			
	}
	
	
	
	public static ArrayList<Annonce> listerAnnonce() {
		session = HibernateUtils.getSession();
		   
		Transaction tx = session.beginTransaction();
		
		Query req = session.createQuery("from Annonce a");
		
		ArrayList<Annonce> listeAnnonce = (ArrayList<Annonce>) req.getResultList();
		
		 tx.commit();
		 session.close();
		
		return listeAnnonce;
	}
	
	public static ArrayList<Annonce> listerAnnonceByName(String name) {
		session = HibernateUtils.getSession();
		   
		Transaction tx = session.beginTransaction();

		Query req = session.getNamedQuery("annonceFindByName");
		req.setParameter("param", name);
		
		ArrayList<Annonce> listeAnnonce = (ArrayList<Annonce>) req.getResultList();
		
		 tx.commit();
		 session.close();
		
		return listeAnnonce;
	}
	
	public static ArrayList<Annonce> listerAnnonceByLvl(String lvl) {
		session = HibernateUtils.getSession();
		   
		Transaction tx = session.beginTransaction();

		Query req = session.getNamedQuery("annonceFindByLvl");
		req.setParameter("param", lvl);
		
		ArrayList<Annonce> listeAnnonce = (ArrayList<Annonce>) req.getResultList();
		
		 tx.commit();
		 session.close();
		
		return listeAnnonce;
	}


	public static void supprimerAnnonce(int id) {
		session = HibernateUtils.getSession();
		   
		Transaction tx = session.beginTransaction();

		Query req = session.getNamedQuery("annonceFindById");
		req.setParameter("param", id);
		
		ArrayList<Annonce> listeAnnonce = (ArrayList<Annonce>) req.getResultList();
		
		for (Annonce annonce : listeAnnonce) {
			session.delete(annonce);
		}
		tx.commit();
		session.close();
	}



	public static void modifierAnnonce(Annonce annonce) {
		session = HibernateUtils.getSession();
		   
		Transaction tx = session.beginTransaction();
		
		session.update(annonce);
		tx.commit();
		session.close();
		
	}

}
