package fr.afpa.dao;

import java.util.ArrayList;
import javax.persistence.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import fr.afpa.beans.Compte;
import fr.afpa.utils.HibernateUtils;

public class DaoCompte {

	private static Session session = null;
	
	public static ArrayList<Compte> lister() {
		session = HibernateUtils.getSession();
		   
		Transaction tx = session.beginTransaction();
		
		Query req = session.createQuery("from Compte c");
		
		ArrayList<Compte> listeCompte = (ArrayList<Compte>) req.getResultList();
		
		 tx.commit();
		 session.close();
		 
		 return listeCompte;
	}
	
	
}
