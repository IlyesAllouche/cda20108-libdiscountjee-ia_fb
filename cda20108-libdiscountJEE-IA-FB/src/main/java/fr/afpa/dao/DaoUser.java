package fr.afpa.dao;

import java.util.ArrayList;

import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.Transaction;

import fr.afpa.beans.Annonce;
import fr.afpa.beans.Utilisateur;
import fr.afpa.utils.HibernateUtils;

public class DaoUser {
	

	private static Session session = null;
	
	
	public static void creer(Utilisateur user) {
		
		 session = HibernateUtils.getSession();
		       
		 Transaction tx = session.beginTransaction();
		 
		 session.save(user);
		 
		 tx.commit();
		 session.close();
			
	}
	
	public static ArrayList<Utilisateur> lister() {
		session = HibernateUtils.getSession();
		   
		Transaction tx = session.beginTransaction();
		
		Query req = session.createQuery("from Utilisateur u");
		
		ArrayList<Utilisateur> listeUser = (ArrayList<Utilisateur>) req.getResultList();
		
		 tx.commit();
		 session.close();
		
		return listeUser;
	}

	public static void modifier(Utilisateur user) {
		session = HibernateUtils.getSession();
		   
		Transaction tx = session.beginTransaction();
		
		
		session.update(user);
		tx.commit();
		session.close();
		
	}
	
	
	public static Utilisateur rechercherUser(int id) {
		session = HibernateUtils.getSession();
		   
		Transaction tx = session.beginTransaction();

		Query req = session.getNamedQuery("findById");
		req.setParameter("param", id);
		
		ArrayList<Utilisateur> listeUser = (ArrayList<Utilisateur>) req.getResultList();
		
		tx.commit();
		session.close();
		
		return listeUser.get(0);
	}
	
	
}
