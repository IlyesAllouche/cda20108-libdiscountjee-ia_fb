package fr.afpa.services;

import java.util.ArrayList;

import fr.afpa.beans.Annonce;
import fr.afpa.dao.DaoAnnonce;

public class ServiceAnnonce {

	/**
	 * Cr�ation d'un annonce par l'utilisateur connect�
	 * @param annonce
	 */
	public static void creerAnnonce(Annonce annonce) {
		DaoAnnonce.creerAnnonce(annonce);
		System.out.println("Annonce cr��e");
	}
	
	
	/**
	 * listage des annocnes de l'utilisateur connect�
	 * @return
	 */
	public static ArrayList<Annonce> listerSelfAnnonces() {
		ArrayList<Annonce>listAnnonce = DaoAnnonce.listerAnnonce();
		
		for (Annonce annonce : listAnnonce) {
			System.out.println(annonce.getNom());
			if (ServiceUser.getUtilisateur() != null && 
					annonce.getUtilisateur().getId() != ServiceUser.getUtilisateur().getId()) {
				listAnnonce.remove(annonce);
			}
		}
		return listAnnonce;
	}
	
	
	/**
	 * Listage de toutes les annonces existante
	 * @return
	 */
	public static ArrayList<Annonce> listerAllAnnonces() {
		ArrayList<Annonce>listAnnonce = DaoAnnonce.listerAnnonce();
		return listAnnonce;
	}
	
	
	/**
	 * Lister les annonces d'un utilisateur via un nom
	 * @param name
	 * @return
	 */
	public static ArrayList<Annonce> listerAnnoncesByName(String name) {
		ArrayList<Annonce>listAnnonce = DaoAnnonce.listerAnnonceByName(name);
		return listAnnonce;
	}
	
	
	/**
	 * Lister les annonces d'un utilisateur via un niveau
	 * @param lvl
	 * @return
	 */
	public static ArrayList<Annonce> listerAnnoncesByLvl(String lvl) {
		ArrayList<Annonce>listAnnonce = DaoAnnonce.listerAnnonceByLvl(lvl);
		return listAnnonce;
	}
	
	
	/**
	 * Modification d'une annonce
	 * @param annonce
	 */
	public static void modifierAnnonce(Annonce annonce) {
		DaoAnnonce.modifierAnnonce(annonce);
	}

	
	/**
	 * Archivage d'une annonce
	 * @param id
	 */
	public static void archiverAnnonce(int id) {
		DaoAnnonce.supprimerAnnonce(id);
	}
	
	
}
