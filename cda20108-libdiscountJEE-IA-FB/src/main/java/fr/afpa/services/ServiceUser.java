package fr.afpa.services;

import java.util.ArrayList;

import fr.afpa.beans.Compte;
import fr.afpa.beans.Utilisateur;
import fr.afpa.dao.DaoCompte;
import fr.afpa.dao.DaoUser;

public class ServiceUser {
	

	/**
	 * Utilisateur connect�
	 */
	private static Utilisateur utilisateur;
	
	/**
	 * Service d'authetification
	 * @param identifiant
	 * @param mdp
	 * @return l'utilisateur connect�
	 */
	public static Utilisateur connexion(String identifiant, String mdp) {
		ArrayList<Compte> listeCompte = DaoCompte.lister();
		
		for (Compte compte : listeCompte) {
			if (compte.getLogin().equals(identifiant) && compte.getMdp().equals(mdp) && compte.getUtilisateur().getDesactive() == 0) {
				System.out.println("Bienvue " + compte.getUtilisateur().getPrenom());
				utilisateur = compte.getUtilisateur();
				return utilisateur;
			}
		}
		
		System.out.println("Aucun compte trouv� avec " + identifiant + " " + mdp);
		return null;
	}
	
	/**
	 * Listage de tous les utilisateurs
	 * @return
	 */
	public static ArrayList<Utilisateur> listeAllUsers() {
		return DaoUser.lister();
	}
	
	/**
	 * creation d'un utilisateur
	 * @param user
	 */
	public static void creerUtilisateur(Utilisateur user) {
		DaoUser.creer(user);
	}

	/**
	 * Modification d'un utilisateur
	 * @param user
	 */
	public static void modifierUtilisateur(Utilisateur user) {
		DaoUser.modifier(user);
	}

	
	/**
	 * D�sactivation d'un utilisateur par l'admin
	 * @param id
	 */
	public static void desactiverUser(int id) {
		Utilisateur temp = DaoUser.rechercherUser(id);
		temp.setDesactive(1);
		DaoUser.modifier(temp);
	}
	
	
	public static Utilisateur getUtilisateur() {
		return utilisateur;
	}



	public static void setUtilisateur(Utilisateur utilisateur) {
		ServiceUser.utilisateur = utilisateur;
	}
	
	
	
	
	
}
