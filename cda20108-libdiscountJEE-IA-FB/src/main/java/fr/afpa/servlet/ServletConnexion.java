package fr.afpa.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.afpa.beans.Utilisateur;
import fr.afpa.services.ServiceUser;
import lombok.Getter;
import lombok.Setter;

/**
 * Servlet implementation class ServletConnexion
 */
public class ServletConnexion extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private static Utilisateur utilisateur;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServletConnexion() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
		
		String login = request.getParameter("login");
		String motDePasse = request.getParameter("mdp");
		
		
		
		if (ServiceUser.connexion(login, motDePasse) != null) {
			if(ServiceUser.getUtilisateur().getAdmin() == 1) {
				RequestDispatcher dispacher = request.getRequestDispatcher("menuAdmin.jsp");
				dispacher.forward(request, response);
			} else {
				RequestDispatcher dispacher = request.getRequestDispatcher("menuUser.jsp");
				dispacher.forward(request, response);
			}
			
		} else {
			RequestDispatcher dispacher = request.getRequestDispatcher("index.jsp");
			dispacher.forward(request, response);
		}
	}
	
	public static Utilisateur getUtilisateur() {
		return utilisateur;
	}

	public static void setUtilisateur(Utilisateur utilisateur) {
		ServletConnexion.utilisateur = utilisateur;
	}
	
	
	
}
