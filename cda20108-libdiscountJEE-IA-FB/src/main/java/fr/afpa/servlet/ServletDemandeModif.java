package fr.afpa.servlet;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.afpa.beans.Annonce;
import fr.afpa.dao.DaoAnnonce;
import fr.afpa.services.ServiceAnnonce;
import fr.afpa.services.ServiceUser;

/**
 * Servlet implementation class ServletDemandeModif
 */
public class ServletDemandeModif extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServletDemandeModif() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.getWriter().append("Served at: ").append(request.getContextPath());
		
		int idAnnonce = Integer.parseInt(request.getParameter("id"));
		
		ArrayList<Annonce>listeAnnonces = ServiceAnnonce.listerAllAnnonces();
		
		Annonce uneAnnonce = null;
		
		for (Annonce annonce : listeAnnonces) {
			if(annonce.getId() == idAnnonce) {
				uneAnnonce = annonce;
				break;
			}
		}
		
		request.setAttribute("annonceModif", uneAnnonce);
		
		RequestDispatcher dispacher = request.getRequestDispatcher("modifierAnnonce.jsp");
		dispacher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
		
		
		
	}

}
