package fr.afpa.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.afpa.beans.Adresse;
import fr.afpa.beans.Compte;
import fr.afpa.beans.Utilisateur;
import fr.afpa.services.ServiceUser;

/**
 * Servlet implementation class ServletInscription
 */
public class ServletInscription extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServletInscription() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
		
		String nom = request.getParameter("nom");
		String prenom = request.getParameter("prenom");
		String mail = request.getParameter("mail");
		String tel = request.getParameter("tel");
		String nomLib = request.getParameter("nomLib");
		
		Utilisateur user = new Utilisateur(nom, prenom, mail, tel, nomLib);
		
		String numRue = request.getParameter("numRue");
		String nomRue = request.getParameter("nomRue");
		String ville = request.getParameter("ville");
		
		Adresse adresse = new Adresse(numRue, nomRue, ville);
		
		String login = request.getParameter("login");
		String motDePasse = request.getParameter("mdp");
		
		Compte compte = new Compte(login, motDePasse);
		
		user.setAdresse(adresse);
		user.setCompte(compte);
		
		ServiceUser.creerUtilisateur(user);
		
		RequestDispatcher dispacher = request.getRequestDispatcher("index.jsp");
		dispacher.forward(request, response);
		
	}
}
