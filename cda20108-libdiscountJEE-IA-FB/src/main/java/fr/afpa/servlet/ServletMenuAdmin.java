package fr.afpa.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.afpa.services.ServiceAnnonce;
import fr.afpa.services.ServiceUser;

/**
 * Servlet implementation class ServletMenuAdmin
 */
public class ServletMenuAdmin extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServletMenuAdmin() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
		
		String choixMenu = request.getParameter("boutonMenu");

		if (choixMenu.equals("Poster une annonce")) {

			RequestDispatcher dispacher = request.getRequestDispatcher("posterAnnonce.jsp");
			dispacher.forward(request, response);
			
			
		} else if (choixMenu.equals("Lister ses annonces")) {
			
			request.setAttribute("listeSesAnnonces", ServiceAnnonce.listerSelfAnnonces());
			
			RequestDispatcher dispacher = request.getRequestDispatcher("listerSesAnnonces.jsp");
			dispacher.forward(request, response);
			
		} else if (choixMenu.equals("Lister toutes les annonces")) {
			
			request.setAttribute("listeToutesAnnonces", ServiceAnnonce.listerAllAnnonces());
			
			RequestDispatcher dispacher = request.getRequestDispatcher("listerToutesAnnonces.jsp");
			dispacher.forward(request, response);
			
		} else if (choixMenu.equals("Rechercher une annonce")) {

			RequestDispatcher dispacher = request.getRequestDispatcher("rechercherAnnonce.jsp");
			dispacher.forward(request, response);

		} else if (choixMenu.equals("Modifier ses informations")) {

			request.setAttribute("utilisateur", ServiceUser.getUtilisateur());
			
			RequestDispatcher dispacher = request.getRequestDispatcher("modifierInfos.jsp");
			dispacher.forward(request, response);

		} else if (choixMenu.equals("Désactiver un utilisateur")) {
			
			request.setAttribute("listeUser", ServiceUser.listeAllUsers());
			
			RequestDispatcher dispacher = request.getRequestDispatcher("desactiverUser.jsp");
			dispacher.forward(request, response);
			
		} else {
			
			request.setAttribute("userCo", ServiceUser.getUtilisateur());

			RequestDispatcher dispacher = request.getRequestDispatcher("consulterInfos.jsp");
			dispacher.forward(request, response);

		}
	}
}
