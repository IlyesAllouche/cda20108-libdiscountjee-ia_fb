package fr.afpa.servlet;

import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.afpa.beans.Annonce;
import fr.afpa.services.ServiceAnnonce;
import fr.afpa.services.ServiceUser;

/**
 * Servlet implementation class ServletModifierAnnonce
 */
public class ServletModifierAnnonce extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServletModifierAnnonce() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
		
		int idAnnonce = Integer.parseInt(request.getParameter("id"));
		
		String nom = request.getParameter("nom");
		String nivSco = request.getParameter("nivSco");
		String isbn = request.getParameter("isbn");
		String dateE = request.getParameter("dateE");
		String maisonE = request.getParameter("maisonE");
		
		String prixU = request.getParameter("prixU");
		String remise = request.getParameter("remise");
		String prixTotal = request.getParameter("prixTotal");
		String quantite = request.getParameter("quantite");
		
		String photo1 = request.getParameter("photo1");
		String photo2 = request.getParameter("photo2");
		String photo3 = request.getParameter("photo3");
		
		ArrayList<Annonce>listeAnnonces = ServiceAnnonce.listerAllAnnonces();
		
		Annonce uneAnnonce = new Annonce();
		
		uneAnnonce.setId(idAnnonce);
		uneAnnonce.setNom(nom);
		uneAnnonce.setNiveauScolaire(nivSco);
		uneAnnonce.setIsbn(isbn);
		uneAnnonce.setDateEdition(LocalDate.parse(dateE));
		uneAnnonce.setMaisonEdition(maisonE);
		
		uneAnnonce.setPrixUnitaire(Float.parseFloat(prixU));
		uneAnnonce.setRemise(Integer.parseInt(remise));
		uneAnnonce.setPrixTotal(Float.parseFloat(prixTotal));
		uneAnnonce.setQuantite(Integer.parseInt(quantite));
		
		uneAnnonce.setPhoto1(photo1);
		uneAnnonce.setPhoto2(photo2);
		uneAnnonce.setPhoto3(photo3);
		
		uneAnnonce.setUtilisateur(ServiceUser.getUtilisateur());
		
		ServiceAnnonce.modifierAnnonce(uneAnnonce);
		
		if (ServiceUser.getUtilisateur().getAdmin() == 1) {
			RequestDispatcher dispacher = request.getRequestDispatcher("menuAdmin.jsp");
			dispacher.forward(request, response);
		} else {
			RequestDispatcher dispacher = request.getRequestDispatcher("menuUser.jsp");
			dispacher.forward(request, response);
		}	
	}
}
