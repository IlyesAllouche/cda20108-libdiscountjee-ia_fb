package fr.afpa.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.afpa.beans.Adresse;
import fr.afpa.beans.Compte;
import fr.afpa.beans.Utilisateur;
import fr.afpa.services.ServiceUser;

/**
 * Servlet implementation class ServletModifierInfos
 */
public class ServletModifierInfos extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ServletModifierInfos() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		String nom = request.getParameter("nom");
		String prenom = request.getParameter("prenom");
		String mail = request.getParameter("mail");
		String tel = request.getParameter("tel");
		String nomLib = request.getParameter("nomLib");
		
		String numRue = request.getParameter("numRue");
		String nomRue = request.getParameter("nomRue");
		String ville = request.getParameter("ville");
		
		ServiceUser.getUtilisateur().setNom(nom);
		ServiceUser.getUtilisateur().setPrenom(prenom);
		ServiceUser.getUtilisateur().setMail(mail);
		ServiceUser.getUtilisateur().setTel(tel);
		ServiceUser.getUtilisateur().setNomLibrairie(nomLib);
		
		ServiceUser.getUtilisateur().getAdresse().setNumero(numRue);
		ServiceUser.getUtilisateur().getAdresse().setNom(nomRue);
		ServiceUser.getUtilisateur().getAdresse().setVille(ville);
		
		ServiceUser.modifierUtilisateur(ServiceUser.getUtilisateur());
		
		if (ServiceUser.getUtilisateur().getAdmin() == 1) {
			RequestDispatcher dispacher = request.getRequestDispatcher("menuAdmin.jsp");
			dispacher.forward(request, response);
		} else {
			RequestDispatcher dispacher = request.getRequestDispatcher("menuUser.jsp");
			dispacher.forward(request, response);
		}	
	}
}
