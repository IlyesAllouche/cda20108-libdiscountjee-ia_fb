package fr.afpa.servlet;

import java.io.IOException;
import java.time.LocalDate;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.afpa.beans.Adresse;
import fr.afpa.beans.Annonce;
import fr.afpa.beans.Compte;
import fr.afpa.beans.Utilisateur;
import fr.afpa.services.ServiceAnnonce;
import fr.afpa.services.ServiceUser;

/**
 * Servlet implementation class ServletPosterAnnonce
 */
public class ServletPosterAnnonce extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServletPosterAnnonce() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
		
		String nom = request.getParameter("nom");
		String nivScolaire = request.getParameter("niveauScolaire");
		String isbn = request.getParameter("isbn");
		LocalDate dateEdition = LocalDate.parse(request.getParameter("dateEdition"));
		String maisonEdition = request.getParameter("maisonEdition");
		
		float prixUni = Float.parseFloat(request.getParameter("prixUnitaire"));
		int remise = Integer.parseInt(request.getParameter("remise"));
		int quantite = Integer.parseInt(request.getParameter("quantite"));
		float prixTotal = (prixUni*(1+(remise/100)))*quantite;
		
		String photo1 = request.getParameter("photo1");
		String photo2 = request.getParameter("photo2");
		String photo3 = request.getParameter("photo3");
		
		Annonce annonce = new Annonce(nom, nivScolaire, isbn, dateEdition, maisonEdition, prixUni, remise, prixTotal, quantite);
		annonce.setPhoto1(photo1);
		annonce.setPhoto2(photo2);
		annonce.setPhoto3(photo3);
		
		annonce.setUtilisateur(ServiceUser.getUtilisateur());
		
		ServiceAnnonce.creerAnnonce(annonce);
		
		if (ServiceUser.getUtilisateur().getAdmin() == 1) {
			RequestDispatcher dispacher = request.getRequestDispatcher("menuAdmin.jsp");
			dispacher.forward(request, response);
		} else {
			RequestDispatcher dispacher = request.getRequestDispatcher("menuUser.jsp");
			dispacher.forward(request, response);
		}	
	}
}
