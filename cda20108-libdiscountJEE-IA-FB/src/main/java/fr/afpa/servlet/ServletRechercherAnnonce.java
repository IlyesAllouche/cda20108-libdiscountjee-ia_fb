package fr.afpa.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.afpa.services.ServiceAnnonce;

/**
 * Servlet implementation class ServletRechercherAnnonce
 */
public class ServletRechercherAnnonce extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServletRechercherAnnonce() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
		
		String choixR = request.getParameter("boutonR");
		
		String mot = request.getParameter("mot");
		
		if(choixR.equals("Rechercher par titre")) {
			
			request.setAttribute("listeBonAnnonces", ServiceAnnonce.listerAnnoncesByName(mot));
			
			RequestDispatcher dispacher = request.getRequestDispatcher("annonceDemande.jsp");
			dispacher.forward(request, response);
			
		} else if(choixR.equals("Rechercher par niveau")) {
			
			request.setAttribute("listeBonAnnonces", ServiceAnnonce.listerAnnoncesByLvl(mot));
			
			RequestDispatcher dispacher = request.getRequestDispatcher("annonceDemande.jsp");
			dispacher.forward(request, response);
			
		}
	}

}
