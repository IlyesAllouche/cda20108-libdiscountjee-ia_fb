package fr.afpa.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.afpa.dao.DaoAnnonce;
import fr.afpa.services.ServiceAnnonce;
import fr.afpa.services.ServiceUser;

/**
 * Servlet implementation class ServletSupprimerAnnonce
 */
public class ServletSupprimerAnnonce extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServletSupprimerAnnonce() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
		
		int idAnnonce = Integer.parseInt(request.getParameter("id"));
		
		ServiceAnnonce.archiverAnnonce(idAnnonce);

		if (ServiceUser.getUtilisateur().getAdmin() == 1) {
			RequestDispatcher dispacher = request.getRequestDispatcher("menuAdmin.jsp");
			dispacher.forward(request, response);
		} else {
			RequestDispatcher dispacher = request.getRequestDispatcher("menuUser.jsp");
			dispacher.forward(request, response);
		}	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
		
		
		
	}

}
