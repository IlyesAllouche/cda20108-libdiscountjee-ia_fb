<%@page import="fr.afpa.beans.Annonce"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Les annonces demandes</title>
<link rel="stylesheet" href="CSS/demande.css">
</head>
<body>

	<h1>
		<strong>Les annonces trouv�es</strong>
	</h1>

	<c:if
		test="${ listeBonAnnonces == null || listeBonAnnonces.size() == 0 }">
		<p class="paraInfos">Il n'y a aucune annonce d'enregistr�e avec ce
			filtre
		<p>
	</c:if>

	<c:forEach items="${ listeBonAnnonces }" var="liste">

		<p class="paraInfos">
			Nom : ${ liste.nom }, Niveau Scolaire : ${ liste.niveauScolaire },
			ISBN : ${ liste.isbn }, Date d'�dition : ${ liste.dateEdition },
			Maison d'�dition : ${ liste.maisonEdition }, Prix unitaire : ${ liste.prixUnitaire },
			Remise : ${ liste.remise }, Quantit� : ${ liste.quantite }, Prix
			Total : ${ liste.prixTotal} <br> <img alt="photo1"
				src="${ liste.photo1 }]"> <img alt="photo2"
				src="${ liste.photo2 }"> <img alt="photo3"
				src="${ liste.photo3 }%>">
		</p>

	</c:forEach>

	<p>
		<a href="ServletDemandeModif?id=${ liste.id }">MODIFIER |</a> <a
			href="ServletSupprimerAnnonce?id=${ liste.id }">| SUPPRIMER</a>
	</p>

	<footer>
		<a id="retour" href="formulaireConnexion.jsp"></a>

	</footer>

</body>
</html>