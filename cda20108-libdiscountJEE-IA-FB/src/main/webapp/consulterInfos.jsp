<%@page import="fr.afpa.beans.Utilisateur"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Consulter ses informations</title>
<link rel="stylesheet" href="CSS/consu-style.css">

</head>
<body>

	<h1>
		<strong>Vos informations</strong>
	</h1>

	<form action="ServletModifierInfos" method="post">
		<div class="container" id="containerInfos">

			<c:set var="user" value="${ userCo }" scope="session"></c:set>

			<p>Nom : ${ user.nom }, Prenom : ${ user.prenom } <br> Mail : ${ user.mail },
				T�l�phone: ${ user.tel } <br> Nom de la librairie : ${ user.nomLibrairie },
				Num�ro de rue : ${ user.adresse.numero } <br> Nom de la rue : ${ user.adresse.nom },
				Ville : ${ user.adresse.ville }</p>

		</div>

	</form>

	<footer>
		<a id="retour" href="formulaireConnexion.jsp">Se d�connecter</a>
	</footer>

</body>
</html>