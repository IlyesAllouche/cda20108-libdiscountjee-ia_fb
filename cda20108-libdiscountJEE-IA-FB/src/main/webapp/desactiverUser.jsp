<%@page import="fr.afpa.beans.Utilisateur"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Desactiver un utilisateur</title>
<link rel="stylesheet" href="CSS/desUser-style.css">
</head>
<body>

	<h1>
		<strong>Désactiver un utilisateur</strong>
	</h1>

	<ul>

		<c:if test="${ listeUser == null || listeUser.size() == 0 }">
			<p class="paraInfos">Il n'y a aucun utilisateur inscrit</p>
		</c:if>

		<c:forEach items="${ listeUser }" var="user">

			<c:if test="${ user.getAdmin() == 0 && user.getDesactive() == 0 }">
				<p class="paraInfos">ID : ${ user.id }, Nom : ${ user.nom },
					Prenom: ${ user.prenom }</p>

				<p>
					<a href="ServletDesactiverUser?id=${ user.id }">Désactiver</a>
				</p>

			</c:if>



		</c:forEach>

	</ul>

	<footer>
		<a id="retour" href="formulaireConnexion.jsp">Se déconnecter</a>
	</footer>

</body>
</html>