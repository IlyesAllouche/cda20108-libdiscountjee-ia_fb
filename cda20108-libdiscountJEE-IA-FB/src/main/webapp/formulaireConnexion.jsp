<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Connexion</title>
<link rel="stylesheet" href="CSS/formulaireConnexion-style.css">
</head>
<body>

	<h1><strong>Connexion</strong></h1>

	<form action="ServletConnexion" method="post">
	
		<div class="container">
		
			<label for="login"></label> 
			<input type="text" placeholder="Login" name="login" required="required">
			
			<label for="mdp"></label> 
			<input type="password"	placeholder="Mot de passe" name="mdp" required="required">
			
		</div>
		
		<div>
			<button type="submit">Se connecter</button>
		</div>
		
	</form>
	
	<footer>
		<a id="retour" href="index.jsp" >Retourner � l'accueil</a>
	</footer>

</body>
</html>