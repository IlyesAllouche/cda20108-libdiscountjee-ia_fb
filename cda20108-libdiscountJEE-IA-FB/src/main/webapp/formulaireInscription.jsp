<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Inscription</title>
<link rel="stylesheet" href="CSS/formulaireInscription-style.css">
</head>
<body>

	<h1><strong>Inscription</strong></h1>

	<form action="ServletInscription" method="post">
		<div class="container" id="containerInfos">
			<label for="nom"><b>Nom : </b></label><input type="text"
				name="nom" required="required"> <br>
			<label for="prenom"><b>Prenom : </b></label><input type="text"
				name="prenom" required="required"> <br>
			<label for="mail"><b>Mail : </b></label><input type="text"
				name="mail" required="required"> <br>
			<label for="tel"><b>T�l�phone : </b></label><input type="text"
				name="tel" required="required"> <br>
			<label for="nomLib"><b>Nom de la librairie : </b></label><input type="text"
				name="nomLib" required="required">
		</div>
		
		<div class="container" id="containerAdresse">
			<label for="numRue"><b>Num�ro de la rue : </b></label><input type="text"
				name="numRue" required="required"> <br>
			<label for="nomRue"><b>Nom de la rue : </b></label><input type="text"
				name="nomRue" required="required"> <br>
			<label for="ville"><b>Ville : </b></label><input type="text"
				name="ville" required="required"> <br>	
			<label for="login"><b>Login : </b></label>
			<input type="text" placeholder="Entrer le login" name="login" required="required"> <br>
			<label for="mdp"><b>Mot de passe : </b></label> 
			<input type="password"	placeholder="Entrer le mot de passe" name="mdp" required="required">
		</div>
	
		<div id="containerBouton">
			<button type="submit">S'inscrire</button>
		</div>
	</form>
	
	<footer>
		<a id="retour" href="index.jsp" >Retourner � l'accueil</a>
	</footer>

</body>
</html>