<%@page import="fr.afpa.beans.Annonce"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Toutes vos annonces</title>
<link rel="stylesheet" href="CSS/liste-style.css">
</head>
<body>

	<h1>
		<strong>Toutes vos annonces</strong>
	</h1>


	<c:if
		test="${ listeSesAnnonces == null || listeSesAnnonces.size() == 0 }">
		<p class="paraInfos">Vous n'avez aucune annonce
		<p>
	</c:if>

	<c:forEach items="${ listeSesAnnonces }" var="liste">

		<p class="paraInfos">
			Nom : ${ liste.nom }, Niveau Scolaire : ${ liste.niveauScolaire },
			ISBN : ${ liste.isbn }, Date d'�dition : ${ liste.dateEdition },
			Maison d'�dition : ${ liste.maisonEdition }, Prix unitaire : ${ liste.prixUnitaire },
			Remise : ${ liste.remise }, Quantit� : ${ liste.quantite }, Prix
			Total : ${ liste.prixTotal} <br> <img alt="photo1"
				src="${ liste.photo1 }]"> <img alt="photo2"
				src="${ liste.photo2 }"> <img alt="photo3"
				src="${ liste.photo3 }%>">
		</p>

	</c:forEach>

	<p>
		<a href="ServletDemandeModif?id=${ liste.id }">MODIFIER |</a> <a
			href="ServletSupprimerAnnonce?id=${ liste.id }">| SUPPRIMER</a>
	</p>
	<footer>
		<a id="retour" href="formulaireConnexion.jsp">Se d�connecter</a>
	</footer>

</body>
</html>