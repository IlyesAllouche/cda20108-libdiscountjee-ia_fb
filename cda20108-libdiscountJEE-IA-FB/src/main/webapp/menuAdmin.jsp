<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Menu</title>
<link rel="stylesheet" href="CSS/menuAdmin-style.css">
</head>
<body>

	<h1><strong>Menu</strong></h1>

	<form action="ServletMenuAdmin" method="post">
		<div id="divUn">
			<input name="boutonMenu" value="Poster une annonce" type="submit" /> <br>
			<input name="boutonMenu" value="Lister ses annonces" type="submit" /> <br>
			<input name="boutonMenu" value="Lister toutes les annonces" type="submit" />
		</div>
		
		<div id="divDeux">
			<input name="boutonMenu" value="Rechercher une annonce" type="submit" /> <br>
			<input name="boutonMenu" value="Modifier ses informations" type="submit" /> <br>
			<input name="boutonMenu" value="Consulter ses informations" type="submit" />
		</div>
		
		<div id="divTrois">
			<input id="boutonFoot" name="boutonMenu" value="Désactiver un utilisateur" type="submit" />
		</div>
	</form>
	
	<footer>
		<a id="retour" href="formulaireConnexion.jsp" >Se déconnecter</a>
	</footer>

</body>
</html>