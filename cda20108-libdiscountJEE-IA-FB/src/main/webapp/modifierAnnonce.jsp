<%@page import="fr.afpa.beans.Annonce"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Modifier mon annonce</title>
<link rel="stylesheet" href="CSS/modifAnnonce-style.css">
</head>
<body>

	<h1>
		<strong>Modifier son annonce</strong>
	</h1>

	
	<c:set var="annonce" value="${ annonceModif }" scope="session"></c:set>

	<form action="ServletModifierAnnonce" method="post">
		<div class="container" id="containerInfos">
		
			<label for="id"><b>Numero : </b></label>
			<input type="text"	name="id" value="${ annonce.id }" required="required" readonly> <br>
		
			<label for="nom"><b>Nom : </b></label>
			<input type="text"	name="nom" value="${ annonce.nom }" required="required"> <br>
			
			<label for="nivSco"><b>Niveau scolaire : </b></label>
			<input type="text"name="nivSco" value="${ annonce.niveauScolaire }" required="required"> <br>
			
			<label for="isbn"><b>ISBN : </b></label>
			<input type="text"name="isbn" value="${ annonce.isbn }" required="required"> <br>
			
			<label for="dateE"><b>Date edition : </b></label>
			<input type="text"name="dateE" value="${ annonce.dateEdition }" required="required"> <br>
			
			<label for="maisonE"><b>Maison edition : </b></label>
			<input type="text"name="maisonE" value="${ annonce.maisonEdition }" required="required"> <br>
			
			<label for="prixU"><b>Prix unitaire : </b></label><input type="text"
				name="prixU" value="${ annonce.prixUnitaire }" required="required"> 
		</div>
		
		<div class="container" id="containerPrix">
			<label for="remise"><b>Remise : </b></label><input type="text"
				name="remise" value="${ annonce.remise }" required="required"> <br>
			<label for="prixTotal"><b>Prix total : </b></label><input type="text"
				name="prixTotal" value="${ annonce.prixTotal }" required="required"> <br>
			<label for="quantite"><b>Quantite : </b></label><input type="text"
				name="quantite" value="${ annonce.quantite }" required="required"> <br>
				<label for="photo1"><b>Photo 1 : </b></label><input type="text"
				name="photo1" value="${ annonce.photo1 }" required="required"> <br>
			<label for="photo2"><b>Photo 2 : </b></label><input type="text"
				name="photo2" value="${ annonce.photo2 }" required="required"> <br>
			<label for="photo3"><b>Photo 3 : </b></label><input type="text"
				name="photo3" value="${ annonce.photo3 }" required="required">
		</div>
		
		<div id="containerBouton">
			<button type="submit">Confirmer les modifications</button>
		</div>
	</form>
	
	<footer>
		<a id="retour" href="formulaireConnexion.jsp">Se déconnecter</a>
	</footer>

</body>
</html>