<%@page import="fr.afpa.beans.Utilisateur"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Modifier ses informations</title>
<link rel="stylesheet" href="CSS/modifInfos-style.css">
</head>
<body>
	<h1>
		<strong>Modifier ses informations</strong>
	</h1>

	
	
	<c:set var="user" value="${ utilisateur }" scope="session"></c:set>

	<form action="ServletModifierInfos" method="post">
		<div class="container" id="containerInfos">
		
			<label for="nom"><b>Nom : &nbsp</b></label>
			<input type="text"	name="nom" value="${ user.id }" required="required"> <br>
			
			<label for="prenom"><b>Prenom : </b></label>
			<input type="text"name="prenom" value="${ user.prenom }" required="required"> <br>
			
			<label for="mail"><b>Mail : </b></label>
			<input type="text"name="mail" value="${ user.mail }" required="required"> <br>
			
			<label for="tel"><b>T�l�phone : </b></label>
			<input type="text"name="tel" value="${ user.tel }" required="required"> <br>
			
			<label for="nomLib"><b>Nom de la librairie : </b></label>
			<input type="text"name="nomLib" value="${ user.nomLibrairie }" required="required">
		</div>
		
		<div class="container" id="containerAdresse">
			<label for="numRue"><b>Num�ro de la rue : </b></label><input type="text"
				name="numRue" value="${ user.adresse.numero }" required="required"> <br>
			<label for="nomRue"><b>Nom de la rue : </b></label><input type="text"
				name="nomRue" value="${ user.adresse.nom }" required="required"> <br>
			<label for="ville"><b>Ville : </b></label><input type="text"
				name="ville" value="${ user.adresse.ville }" required="required">
		</div>
		
		<div id="containerBouton">
			<button type="submit">Confirmer les modifications</button>
		</div>
	</form>
	
	<footer>
		<a id="retour" href="formulaireConnexion.jsp">Se d�connecter</a>
	</footer>

</body>
</html>