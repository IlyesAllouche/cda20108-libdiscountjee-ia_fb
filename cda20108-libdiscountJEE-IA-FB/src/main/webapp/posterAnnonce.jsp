<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Poster une annonce</title>
<link rel="stylesheet" href="CSS/posterAnnonce-style.css">
</head>
<body>

	<h1>
		<strong>Poster votre annonce</strong>
	</h1>

	<form action="ServletPosterAnnonce" method="post">
		<div class="container" id="containerInfos">
			<label for="nom"><b>Nom : </b></label><input type="text" name="nom"
				required="required"> <br> <label for="niveauScolaire"><b>Niveau
					scolaire : </b></label><input type="text" name="niveauScolaire"
				required="required"> <br> <label for="isbn"><b>ISBN
					: </b></label><input type="text" name="isbn" required="required"> <br>
			<label for="dateEdition"><b>Date d'�dition : </b></label><input
				type="text" name="dateEdition" required="required"> <br>
			<label for="maisonEdition"><b>Maison d'�dition : </b></label><input
				type="text" name="maisonEdition" required="required"> <br>
			<label for="prixUnitaire"><b>Prix unitaire : </b></label><input
				type="text" name="prixUnitaire" required="required">
		</div>

		<div class="container" id="containerPrix">
			<label for="remise"><b>Remise : </b></label><input type="text"
				name="remise" required="required"> <br> <label
				for="quantite"><b>Quantit� : </b></label><input type="text"
				name="quantite" required="required"> <br> <label
				for="photo1"><b>Photo 1 : </b></label> <input type="text"
				name="photo1"> <br> <label for="photo2"><b>Photo
					2 : </b></label> <input type="text" name="photo2"> <br> <label
				for="photo3"><b>Photo 3 : </b></label> <input type="text"
				name="photo3">
		</div>

		<div id="containerBouton">
			<button type="submit">Poster</button>
		</div>
	</form>

	<footer>
		<a id="retour" href="formulaireConnexion.jsp">Se d�connecter</a>
	</footer>

</body>
</html>