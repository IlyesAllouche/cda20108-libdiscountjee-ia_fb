<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Rechercher une annonce</title>
<link rel="stylesheet" href="CSS/recherche-style.css">
</head>
<body>

	<h1><strong>Rechercher une annonce</strong></h1>

	<form action="ServletRechercherAnnonce" method="post">
	
		<div class="container">
		
			<label for="mot"></label> 
			<input type="text" placeholder="Votre filtre" name="mot" required="required">
			
		</div>
		
		<div>
			<input id="boutonTitre" class="boutons" name="boutonR" value="Rechercher par titre" type="submit" />
			<input id="boutonLvl" class="boutons" name="boutonR" value="Rechercher par niveau" type="submit" />
		</div>
		
	</form>
	
	<footer>
		<a id="retour" href="formulaireConnexion.jsp" >Se déconnecter</a>
	</footer>
</body>
</html>